# 执行出错参考 https:/go.microsoft.com/fwlink/?LinkID=135170
# Get-ExecutionPolicy
# Get-ExecutionPolicy -List
# Get-ExecutionPolicy -Scope CurrentUser
# AllSigned、Bypass、Default、RemoteSigned、Restricted、Undefined、Unrestricted
# Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
# Set-ExecutionPolicy -ExecutionPolicy Undefined -Scope CurrentUser

dotnet --list-sdks
dotnet --list-runtimes

# 查看版本
dotnet ef -v
# 安装dotnet ef
dotnet tool install --global dotnet-ef --version 8.0.0
# 更新dotnet ef
dotnet tool update --global dotnet-ef
dotnet tool update --global dotnet-ef --version 8.0.0
# 卸载 dotnet ef
dotnet tool uninstall --global dotnet-ef

# 添加迁移
# dotnet ef migrations add init1 -o .\Infrastructure\Data --project "../src/JackProjectTemplate.EntityFrameworkCore" --startup-project "../src/JackProjectTemplate.Web" --context JackProjectTemplateDbContext --no-build
# dotnet ef migrations add init2 -o .\Infrastructure\Data --project "../src/JackProjectTemplate.EntityFrameworkCore" --startup-project "../src/JackProjectTemplate.Web" --context JackProjectTemplateDbContext --no-build

# https://learn.microsoft.com/zh-cn/ef/core/cli/dotnet
# https://learn.microsoft.com/zh-cn/dotnet/core/rid-catalog
# https://learn.microsoft.com/zh-cn/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli

# 删除迁移
dotnet ef migrations remove --project "../src/JackProjectTemplate.EntityFrameworkCore" --startup-project "../src/JackProjectTemplate.Web" --context JackProjectTemplateDbContext

# 生成数据库脚本
dotnet ef migrations script init1 init2 -o migration-script.sql --project "../src/JackProjectTemplate.EntityFrameworkCore" --startup-project "../src/JackProjectTemplate.Web" --context JackProjectTemplateDbContext
dotnet ef migrations script -o migration-script.sql --project "../src/JackProjectTemplate.EntityFrameworkCore" --startup-project "../src/JackProjectTemplate.Web" --context JackProjectTemplateDbContext
