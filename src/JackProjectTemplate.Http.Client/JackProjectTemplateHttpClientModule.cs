﻿using Microsoft.Extensions.DependencyInjection;

using JackProjectTemplate.Ability.Docking;

using Youshow.Ace.Http.Client;
using Youshow.Ace.Modularity;

namespace JackProjectTemplate.Http.Client
{
    [RelyOn(
        typeof(AceHttpClientModule)
    )]
    public class JackProjectTemplateHttpClientModule : AceModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            ArgumentNullException.ThrowIfNull(context);

            context.Services.AddAceHttpClient(opts =>
            {
                opts.AddRemoteModule<JackProjectTemplateAbilityDockingModule>("Default");
            });
        }
    }
}
