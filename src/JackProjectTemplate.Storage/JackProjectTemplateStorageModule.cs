﻿using Microsoft.Extensions.Configuration;

using JackProjectTemplate.Shared;
using JackProjectTemplate.Storage.OSS;

using Youshow.Ace.Modularity;
using JackProjectTemplate.Shared.Extensions;

namespace JackProjectTemplate.Storage
{
    [RelyOn(
        typeof(JackProjectTemplateSharedModule)
    )]
    public class JackProjectTemplateStorageModule : AceModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            ArgumentNullException.ThrowIfNull(context);

            ConfigurationManager configurationManager = context.Services.GetConfigurationManager();
            Configure<OSSOptions>(configurationManager.GetRequiredSection("OSSOption"));
        }
    }
}