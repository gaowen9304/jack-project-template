﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Options;

namespace JackProjectTemplate.Storage.OSS
{
    public class OSSOptions
    {
        public required string AccessKeyId { get; set; }
        public required string AccessKeySecret { get; set; }

        public required string Endpoint { get; set; }

        public required string BucketName { get; set; }

        public required string Host { get; set; }
    }
}
