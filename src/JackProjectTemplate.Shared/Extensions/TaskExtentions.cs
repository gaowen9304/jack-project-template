﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Shared.Extensions
{
    public static class TaskExtentions
    {
        public static Task<TNewResult> ContinueWithUnwrap<TResult, TNewResult>(this Task<TResult> task, Func<Task<TResult>, Task<TNewResult>> continuationFunction)
        {
            ArgumentNullException.ThrowIfNull(task);
            ArgumentNullException.ThrowIfNull(continuationFunction);

            return task.ContinueWith(continuationFunction).Unwrap();
        }
    }
}
