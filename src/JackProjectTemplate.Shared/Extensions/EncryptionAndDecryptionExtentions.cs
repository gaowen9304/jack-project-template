﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Shared.Extensions
{
    public static class EncryptionAndDecryptionExtentions
    {
        public static string ToHmac(this string plainText, string key = "defaultKey")
        {
            using var h = new HMACSHA256(Encoding.UTF8.GetBytes(key));
            var sum = h.ComputeHash(Encoding.UTF8.GetBytes(plainText));

            var sb = new StringBuilder();
            foreach (byte b in sum)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }

        public static string ToMd5(this string input)
        {
            byte[] data = MD5.HashData(Encoding.Default.GetBytes(input));
            StringBuilder sBuilder = new();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        public static string ToSHA256(this string input)
        {
            return string.Concat(SHA256.HashData(Encoding.UTF8.GetBytes(input)).Select(item => item.ToString("x2")));
        }
    }
}
