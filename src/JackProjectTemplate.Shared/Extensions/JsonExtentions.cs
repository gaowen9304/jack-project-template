﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackProjectTemplate.Shared.Utilities;

namespace JackProjectTemplate.Shared.Extensions
{
    public static class JsonExtentions
    {
        public static string ToJson<T>(this T? obj)
        {
            return JSON.Serialize(obj);
        }

        public static T? ToObj<T>(this string? json)
        {
            if (json == null) return default;

            return JSON.Deserialize<T>(json);
        }
    }
}
