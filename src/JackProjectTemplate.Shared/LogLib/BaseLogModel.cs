﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Shared.LogLib
{
    public abstract class BaseLogModel
    {
        private static readonly Type _ignoreAttributeType = typeof(IgnoreAttribute);

        public override string ToString()
        {
            StringBuilder msgBuilder = new();
            msgBuilder.Append($"[{GetType().FullName}]{{");

            bool first = true;
            foreach (var propertyInfo in GetType().GetProperties())
            {
                if (propertyInfo.IsDefined(_ignoreAttributeType, false)) continue;

                if (first)
                {
                    first = false;
                    msgBuilder.Append($" {propertyInfo.Name} = {Convert.ToString(propertyInfo.GetValue(this))}");
                }
                else
                {
                    msgBuilder.Append($", {propertyInfo.Name} = {Convert.ToString(propertyInfo.GetValue(this))}");
                }
            }

            msgBuilder.Append(" }");

            return msgBuilder.ToString();
        }
    }
}
