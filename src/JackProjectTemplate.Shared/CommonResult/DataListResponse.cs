﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Shared.CommonResult
{
    public class DataListResponse<TData> : BaseResult
    {
        public DataList<TData>? Data { get; set; }
        public StatisticData? Statistics { get; set; }
    }

    public class DataList<TData>
    {
        public IEnumerable<TData>? List { get; set; }
    }

    public class StatisticData
    {
        public int Total { get; set; }
    }
}
