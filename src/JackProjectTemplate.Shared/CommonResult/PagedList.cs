﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Shared.CommonResult
{
    public class PagedList<TData> : IPagedList<TData>
    {
        public PagedList() { }

        public PagedList(IList<TData>? items, int pageIndex, int pageSize, int totalCount)
        {
            Current = pageIndex;
            PageSize = pageSize;
            Total = totalCount;
            PageTotal = (int)Math.Ceiling(totalCount / (double)pageSize);
            List = items;
        }

        public int Current { get; set; }

        public int PageSize { get; set; }

        public int Total { get; set; }

        public int PageTotal { get; set; }

        public IEnumerable<TData>? List { get; set; }

        public static PagedList<TData> Create(IPagedList<TData> source)
        {
            ArgumentNullException.ThrowIfNull(source);

            if (source is PagedList<TData> same) return same;
            return new PagedList<TData>(source?.List?.ToList(), source?.Current ?? 0, source?.PageSize ?? 0, source?.Total ?? 0);
        }
    }

    public interface IPagedList<out TData>
    {
        int Current { get; }

        int PageSize { get; }

        int Total { get; }

        int PageTotal { get; }

        IEnumerable<TData>? List { get; }
    }
}
