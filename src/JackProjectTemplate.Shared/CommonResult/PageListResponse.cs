﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Shared.CommonResult
{
    public class PageListResponse<TData> : BaseResult
    {
        public PagedList<TData>? Data { get; set; }
        public StatisticData? Statistics { get; set; }
    }

    public class PageListResponse<TData, TStatistics> : BaseResult
    {
        public PagedList<TData>? Data { get; set; }
        public TStatistics? Statistics { get; set; }
    }
}
