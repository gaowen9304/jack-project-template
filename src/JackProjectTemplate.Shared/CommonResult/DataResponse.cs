﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Shared.CommonResult
{
    public class DataResponse<TData> : BaseResult
    {
        public DataResponse()
        {
            
        }

        public DataResponse(TData? data)
        {
            Data = data;
        }

        public TData? Data { get; set; }

        public new static DataResponse<TData> CreateFailureResult(string msg)
        {
            return new DataResponse<TData> { Code = CommonResultCode.FAILURE, Message = msg, MessageType = MessageType.Error };
        }

        public static DataResponse<TData> CreateSuccessResult(TData data, string msg = "")
        {
            return new DataResponse<TData> { Message = msg, Data = data };
        }
    }

    public class DataResponse : BaseResult
    {
        public DataResponse()
        {

        }

        public DataResponse(object? data)
        {
            Data = data;
        }

        public object? Data { get; set; }

        public new static DataResponse CreateFailureResult(string msg)
        {
            return new DataResponse { Code = CommonResultCode.FAILURE, Message = msg, MessageType = MessageType.Error };
        }

        public new static DataResponse CreateSuccessResult(string msg)
        {
            return new DataResponse { Message = msg, };
        }
    }
}
