﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Shared.CommonResult
{
    public class BaseResult
    {
        /// <summary>
        /// 响应编号
        /// </summary>
        public string Code { get; set; } = CommonResultCode.SUCCESS;
        /// <summary>
        /// 消息
        /// </summary>
        public string? Message { get; set; }
        /// <summary>
        /// 消息类型
        /// </summary>
        public MessageType MessageType { get; set; } = MessageType.Success;

        public static BaseResult CreateFailureResult(string msg)
        {
            return new BaseResult { Code = CommonResultCode.FAILURE, Message = msg, MessageType = MessageType.Error };
        }

        public static BaseResult CreateSuccessResult(string msg)
        {
            return new BaseResult { Message = msg, };
        }
    }
}
