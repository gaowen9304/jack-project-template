﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Shared.CommonResult
{
    public static class CommonResultCode
    {
        public const string SUCCESS = "0";
        public const string FAILURE = "-1";
    }
}
