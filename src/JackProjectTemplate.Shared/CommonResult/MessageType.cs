﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Shared.CommonResult
{
    public enum MessageType
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        None = 0,
        /// <summary>
        /// 信息
        /// </summary>
        [Description("信息")]
        Info = 1,
        /// <summary>
        /// 警告
        /// </summary>
        [Description("警告")]
        Warn = 2,
        /// <summary>
        /// 错误
        /// </summary>
        [Description("错误")]
        Error = 3,
        /// <summary>
        /// 成功
        /// </summary>
        [Description("成功")]
        Success = 4,
    }
}
