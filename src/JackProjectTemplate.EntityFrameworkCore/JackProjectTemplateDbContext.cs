﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Youshow.Ace.Data;
using Youshow.Ace.EntityFrameworkCore;

namespace JackProjectTemplate.EntityFrameworkCore
{
    [ConnectionStringName("Default")]
    public class JackProjectTemplateDbContext(DbContextOptions<JackProjectTemplateDbContext> options) : JackDbContext<JackProjectTemplateDbContext>(options)
    {
    }
}
