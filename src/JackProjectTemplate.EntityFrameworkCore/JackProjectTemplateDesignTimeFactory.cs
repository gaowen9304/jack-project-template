﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using Youshow.Ace.EntityFrameworkCore;
using Youshow.Ace.EntityFrameworkCore.PostgreSql;

namespace JackProjectTemplate.EntityFrameworkCore
{
    public class JackProjectTemplateDesignTimeFactory : AcePostgreSqlDesignTimeDbContextFactory<JackProjectTemplateDbContext>
    {
        public override AceDesignTimeDbContextOptions Options => new()
        {
            StartupProjectPath = @"./", // appsetting.json所在目录
        };

        protected override JackProjectTemplateDbContext CreateDbContext(IConfigurationRoot configuration)
        {
            return base.CreateDbContext(configuration);
        }
    }
}

// https://learn.microsoft.com/zh-cn/ef/core/cli/dbcontext-creation?tabs=dotnet-core-cli
