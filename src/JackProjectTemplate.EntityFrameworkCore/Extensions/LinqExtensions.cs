﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.EntityFrameworkCore.Extensions
{
    public static class LinqExtensions
    {
        public static IQueryable<TSource> WhereIf<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, Func<bool> condition)
        {
            ArgumentNullException.ThrowIfNull(condition);

            if (condition() == false) return source;

            return source.Where(predicate);
        }

        public static IQueryable<TSource> WhereIf<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, bool condition = true)
        {
            if (condition == false) return source;

            return source.Where(predicate);
        }
    }
}
