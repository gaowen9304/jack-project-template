﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using JackProjectTemplate.Domain;

using Youshow.Ace.EntityFrameworkCore;
using Youshow.Ace.Modularity;

namespace JackProjectTemplate.EntityFrameworkCore
{
    [RelyOn(
        typeof(AceEntityFrameworkCoreModule),
        typeof(JackProjectTemplateDomainModule)
    )]
    public class JackProjectTemplateEntityFrameworkCoreModule : AceModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            ArgumentNullException.ThrowIfNull(context);

            context.Services.AddAceDbContext<JackProjectTemplateDbContext>(opts =>
            {
                opts.AddDefaultRepositories(true);
            });

            Configure<AceDbContextOptions>(opts =>
            {
                opts.UseNpgsql();

                //opts.PreConfigure(options =>
                //{
                //    options.DbContextOptions
                //        .UseLoggerFactory(options.ServiceProvider.GetRequiredService<ILoggerFactory>())
                //        .EnableSensitiveDataLogging()
                //        .EnableDetailedErrors()
                //        .UseNpgsql();
                //});
            });
        }
    }
}
