﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Youshow.Ace.EntityFrameworkCore;

namespace JackProjectTemplate.EntityFrameworkCore
{
    public abstract class JackDbContext<TDbContext> : AceDbContext<TDbContext>
        where TDbContext : DbContext
    {
        protected JackDbContext(DbContextOptions<TDbContext> options) : base(options)
        {
        }

        public virtual IQueryable<TEntity> Query<TEntity>()
            where TEntity : class
        {
            return this.Set<TEntity>().AsNoTracking();
        }
    }
}
