using Serilog;

using JackProjectTemplate.Web;

try
{
    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
    AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);

    await WebApplication.CreateBuilder(args).BuildApplication<JackProjectTemplateWebModule>().RunAsync();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host failure.");
}
finally
{
    await Log.CloseAndFlushAsync();
}
