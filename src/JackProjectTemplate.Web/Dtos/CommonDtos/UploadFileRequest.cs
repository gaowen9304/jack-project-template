﻿using System.ComponentModel.DataAnnotations;

namespace JackProjectTemplate.Web.Dtos.CommonDtos
{
    public class UploadFileRequest
    {
        /// <summary>
        /// 文件
        /// </summary>
        [Required]
        public required IFormFile File {  get; set; }
        /// <summary>
        /// 类型: picture, video, file
        /// </summary>
        [Required]
        public required string Type { get; set; }
    }
}
