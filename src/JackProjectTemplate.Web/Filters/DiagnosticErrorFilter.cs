﻿using System.Diagnostics;
using System.Runtime.CompilerServices;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace JackProjectTemplate.Web.Filters
{
    public class DiagnosticErrorFilter(ILogger<DiagnosticErrorFilter> logger) : IAlwaysRunResultFilter, IResultFilter, IFilterMetadata, IOrderedFilter
    {
        [CompilerGenerated]
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly ILogger<DiagnosticErrorFilter> _logger = logger;

        public int Order { get; set; } = 5000;

        public void OnResultExecuted(ResultExecutedContext context)
        {
        }

        public void OnResultExecuting(ResultExecutingContext context)
        {
            if (context?.Result is ObjectResult objectResult && objectResult.Value is ProblemDetails problemDetails)
            {
                string message = $"Title：{problemDetails.Title}{Environment.NewLine}Detail：{problemDetails.Detail}";
                _logger.LogError(message, []);
            }
        }
    }
}
