﻿using Jack.RedisLib;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using RedisLib;

using JackProjectTemplate.Shared;
using JackProjectTemplate.Shared.Extensions;

using Youshow.Ace.Modularity;

namespace JackProjectTemplate.Cache
{
    [RelyOn(
        typeof(JackProjectTemplateSharedModule)
    )]
    public class JackProjectTemplateCacheModule : AceModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            ArgumentNullException.ThrowIfNull(context);

            //ConfigurationManager? configurationManager = context.Services.FirstOrDefault(x => x.ServiceType == typeof(IConfiguration))?.ImplementationInstance as ConfigurationManager;
            //ArgumentNullException.ThrowIfNull(configurationManager);
            ConfigurationManager configurationManager = context.Services.GetConfigurationManager();

            Configure<RedisOptions>(configurationManager.GetRequiredSection("JackRedisOption"));
            context.Services.AddSingleton<IRedisClient, RedisClient>();
            context.Services.AddSingleton<IRedisLock, RedisLock>();
        }
    }
}
