﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Cache.DistributedIdentifyGeneraters.SequentialGuid
{
    public sealed class SequentialGuidGenerator : SequentialGuidGeneratorBase<SequentialGuidGenerator>
    {
        private SequentialGuidGenerator() { }
    }
}
