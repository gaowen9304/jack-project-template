﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Youshow.Ace.DependencyInjection;

namespace JackProjectTemplate.Cache.DistributedIdentifyGeneraters.SequentialGuid
{
    internal class SequentialIdGenerator : IIdGenerator
    {
        public Guid NewId() => SequentialGuidGenerator.Instance.NewGuid();
    }

    public interface IIdGenerator : ITransientDependency
    {
        Guid NewId();
    }
}
