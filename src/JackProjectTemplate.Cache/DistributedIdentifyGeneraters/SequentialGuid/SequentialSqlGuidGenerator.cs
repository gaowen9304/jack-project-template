﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Cache.DistributedIdentifyGeneraters.SequentialGuid
{
    public sealed class SequentialSqlGuidGenerator : SequentialGuidGeneratorBase<SequentialSqlGuidGenerator>
    {
        private SequentialSqlGuidGenerator() { }

        internal override Guid NewGuid(long timestamp) =>
         base.NewGuid(timestamp).ToSqlGuid().Value;

        public SqlGuid NewSqlGuid() =>
            new(NewGuid());

        public SqlGuid NewSqlGuid(DateTime timestamp) =>
            new(NewGuid(timestamp));
    }
}
