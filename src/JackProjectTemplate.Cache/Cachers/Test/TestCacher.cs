﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Jack.RedisLib;

using JackProjectTemplate.Cache.Ctos.Test;

using Youshow.Ace.DependencyInjection;

namespace JackProjectTemplate.Cache.Cachers.Test
{
    internal class TestCacher(IRedisClient redisClient) : ITestCacher
    {
        #region 缓存key模板
        /// <summary>
        /// 测试缓存key模板
        /// 0：数据标识
        /// </summary>
        private const string _cacheKeyTemplate = "test:{0}";
        #endregion

        public async Task<bool> SetCache(TestCto testCto)
        {
            string cacheKey = string.Format(_cacheKeyTemplate, testCto.Id);
            return await redisClient.StringSet(cacheKey, testCto, TimeSpan.FromHours(1));
        }

        public async Task<TestCto> GetCache(long id)
        {
            string cacheKey = string.Format(_cacheKeyTemplate, id);
            return await redisClient.StringGet<TestCto>(cacheKey);
        }
    }

    public interface ITestCacher : ITransientDependency
    {
        Task<TestCto> GetCache(long id);
        Task<bool> SetCache(TestCto testCto);
    }
}
