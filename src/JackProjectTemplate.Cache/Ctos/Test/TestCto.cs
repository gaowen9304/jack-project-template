﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Nelibur.ObjectMapper;

using JackProjectTemplate.Domain.Data.Test;
using JackProjectTemplate.Shared.LogLib;

namespace JackProjectTemplate.Cache.Ctos.Test
{
    public class TestCto : BaseLogModel
    {
        static TestCto()
        {
            TinyMapper.Bind<TestEntity, TestCto>();
            TinyMapper.Bind<TestCto, TestEntity>(config =>
            {
                config.Ignore(x => x.Id);
            });
        }

        public TestCto()
        {

        }

        public TestCto(TestEntity testEntity)
        {
            _ = TinyMapper.Map(testEntity, this);
        }

        public TestEntity ToEntity()
        {
            return TinyMapper.Map(this, new TestEntity(this.Id));
        }

        public long Id { get; set; }
        public string? Name { get; set; }
        public int? Age { get; set; }
        public DateTimeOffset? BirthDay { get; set; }
    }
}
