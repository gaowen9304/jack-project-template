﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JackProjectTemplate.Domain.Data
{
    public partial class JackColumnAttribute : ColumnAttribute
    {
        public JackColumnAttribute([NotNull] string name)
            : base(ConvertName(name))
        {
        }

        protected static string ConvertName(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return name;

            string[] words = _nameSplitRegex().Split(name);

            return string.Join("_", words.Select(w => $"{char.ToLower(w[0])}{w[1..]}"));
        }

        [GeneratedRegex(@"(?<!^)(?=[A-Z])")]
        private static partial Regex _nameSplitRegex();
    }
}
