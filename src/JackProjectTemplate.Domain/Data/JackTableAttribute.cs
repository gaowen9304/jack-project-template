﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace JackProjectTemplate.Domain.Data
{
    public class JackTableAttribute(string name) : TableAttribute(ConvertName(name))
    {
        protected static string ConvertName(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return name;

            name = name.ToLower();
            if (name.EndsWith("entity"))
                name = name[..^"entity".Length];

            return name;
        }
    }
}
