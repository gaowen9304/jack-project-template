﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Youshow.Ace.Domain.Models;

namespace JackProjectTemplate.Domain.Data.Test
{
    //[Table("test")]
    [JackTable(nameof(TestEntity))]
    public class TestEntity : JackBaseModel<long>
    {
        public TestEntity()
        {
            
        }

        public TestEntity(long id)
        {
            Id = id;
        }

        [JackColumn(nameof(Name))]
        //[Column("name")]
        public string? Name { get; set; }
        [JackColumn(nameof(Age))]
        //[Column("age")]
        public int? Age { get; set; }
        [JackColumn(nameof(BirthDay))]
        //[Column("birth_day")]
        public DateTimeOffset? BirthDay { get; set; }
    }
}
