﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Youshow.Ace.Domain.Models;

namespace JackProjectTemplate.Domain.Data
{
    public abstract class JackBaseModel<TKey>: BaseModel<TKey>
    {
        [Key]
        //[JackColumn(nameof(Id))]
        [Column("id")]
        public override TKey Id { get => base.Id; protected set => base.Id = value; }
    }
}
