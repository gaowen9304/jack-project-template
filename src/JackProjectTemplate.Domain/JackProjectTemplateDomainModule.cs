﻿using Youshow.Ace.Domain;
using Youshow.Ace.Modularity;

namespace JackProjectTemplate.Domain
{
    [RelyOn(
    typeof(AceDomainModule)
    )]
    public class JackProjectTemplateDomainModule : AceModule
    {

    }
}
