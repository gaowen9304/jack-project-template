﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Ability.Docking.Dtos.TestDtos
{
    public class TestCreateDto
    {
        [Required]
        public required string Name { get; set; }
        [Required]
        [Range(0, 130)]
        public required int Age { get; set; }
    }
}
