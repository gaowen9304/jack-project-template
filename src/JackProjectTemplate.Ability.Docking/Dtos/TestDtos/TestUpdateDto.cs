﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackProjectTemplate.Ability.Docking.Dtos.TestDtos
{
    public class TestUpdateDto
    {
        public string? Name { get; set; }
        public int? Age { get; set; }
    }
}
