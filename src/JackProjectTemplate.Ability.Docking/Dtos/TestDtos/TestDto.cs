﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Youshow.Ace.Ability.Dtos;

namespace JackProjectTemplate.Ability.Docking.Dtos.TestDtos
{
    public class TestDto : IBaseModelDto<long>
    {
        /// <summary>
        /// 数据标识
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string? Name { get; set; }
        /// <summary>
        /// 年龄
        /// </summary>
        public int? Age { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public DateTimeOffset? BirthDay { get; set; }
    }
}
