﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackProjectTemplate.Ability.Docking.Dtos.TestDtos;
using Youshow.Ace;
using Youshow.Ace.Ability.Dtos;
using Youshow.Ace.Ability.Services;

namespace JackProjectTemplate.Ability.Docking.Test
{
    /// <summary>
    /// 测试自动api
    /// </summary>
    //[NonRemoteVisible]
    public interface ITestAutoApiService : IAutoAbilityServicer<TestDto, long, PageSortRequestDto, TestCreateDto, TestUpdateDto>
    {

    }
}
