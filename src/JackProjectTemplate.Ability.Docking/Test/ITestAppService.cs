﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using JackProjectTemplate.Ability.Docking.Dtos.TestDtos;
using JackProjectTemplate.Shared.CommonResult;

using Youshow.Ace.Ability.Services;
using Youshow.Ace.DependencyInjection;

namespace JackProjectTemplate.Ability.Docking.Test
{
    public interface ITestAppService : IAbilityServicer
    {
        /// <summary>
        /// 获取测试数据
        /// </summary>
        /// <param name="id">数据标识</param>
        /// <returns>测试数据</returns>
        Task<DataResponse<TestDto>> Get(long id);
    }
}
