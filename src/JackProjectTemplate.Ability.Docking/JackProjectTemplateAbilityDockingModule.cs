﻿using Youshow.Ace.Ability;
using Youshow.Ace.Modularity;

namespace JackProjectTemplate.Ability.Docking
{
    [RelyOn(
        typeof(AceAbilityDockingModule)
    )]
    public class JackProjectTemplateAbilityDockingModule : AceModule
    {

    }
}
