﻿using Microsoft.Extensions.DependencyInjection;

using Serilog.Core.Enrichers;
using Serilog.Enrichers.AspNetCore.HttpContext;

using JackProjectTemplate.Ability.Docking;
using JackProjectTemplate.Cache;
using JackProjectTemplate.EntityFrameworkCore;
using JackProjectTemplate.Shared.Extensions;

using Youshow.Ace;
using Youshow.Ace.Ability;
using Youshow.Ace.AutoMapper;
using Youshow.Ace.Modularity;

namespace JackProjectTemplate.Ability
{
    [RelyOn(
    typeof(AceAbilityModule),
        typeof(AceAutoMapperModule),
        typeof(JackProjectTemplateAbilityDockingModule),
        typeof(JackProjectTemplateEntityFrameworkCoreModule),
        typeof(JackProjectTemplateCacheModule)
    )]
    public class JackProjectTemplateAbilityModule : AceModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            ArgumentNullException.ThrowIfNull(context);

            context.Services.Configure<AceAutoMapperOptions>(opts =>
            {
                opts.StartAddProfileToMapper(true);
            });
        }
    }
}

/*
https://www.qubcedu.com/AceFxDoc/8.0.0/2131
https://devv.ai/zh/search?threadId=da1fqtil9l34
https://devv.ai/zh/search?threadId=d9vcyv8pubr4
https://devv.ai/zh/search?threadId=da1ge1g6h69s
https://devv.ai/zh/search?threadId=da1gmouna3nk
https://github.com/serilog/serilog/wiki/Structured-Data
https://github.com/serilog/serilog/wiki/Configuration-Basics
https://github.com/serilog/serilog
https://serilog.net/
Elasticsearch生命周期策略ilm_policy、索引模板template管理（一）https://blog.csdn.net/WoAiShuiGeGe/article/details/107078927
https://blog.csdn.net/zhangpfly/article/details/109238869


索引管理 /app/management/data/index_management/indices
索引模板 /app/management/data/index_management/templates
索引生命周期策略 /app/management/data/index_lifecycle_management/policies
索引模式 /app/management/kibana/indexPatterns
开发工具 /app/dev_tools#/console
Discover（数据查询） /app/discover


步骤一：在Elasticsearch中创建索引生命周期策略
PUT _ilm/policy/serilog-events-ilm-policy
{"policy":{"phases":{"hot":{"min_age":"0ms","actions":{"set_priority":{"priority":100}}},"warm":{"min_age":"15d","actions":{"allocate":{"include":{"box_type":"warm"},"exclude":{},"require":{}},"forcemerge":{"max_num_segments":1},"set_priority":{"priority":50}}},"cold":{"min_age":"30d","actions":{"allocate":{"include":{"box_type":"cold"},"exclude":{},"require":{}}}},"delete":{"min_age":"90d","actions":{"delete":{"delete_searchable_snapshot":true}}}}}}

步骤二：将索引模板与生命周期策略关联
PUT _template/serilog-events-index-template?include_type_name
{"order":0,"index_patterns":["serilog-*"],"aliases":{"serilog-log":{}},"settings":{"index":{"refresh_interval":"5s","number_of_shards":"1","number_of_replicas":"1","lifecycle":{"name":"serilog-events-ilm-policy"}}},"mappings":{"_doc":{"dynamic":true,"numeric_detection":false,"date_detection":true,"dynamic_date_formats":["strict_date_optional_time","yyyy/MM/dd HH:mm:ss Z||yyyy/MM/dd Z"],"_source":{"enabled":true,"includes":[],"excludes":[]},"_routing":{"required":false},"dynamic_templates":[{"numerics_in_fields":{"match_pattern":"regex","path_match":"fields\\.[\\d+]$","mapping":{"norms":false,"index":true,"type":"text"}}},{"string_fields":{"mapping":{"norms":false,"index":true,"type":"text","fields":{"raw":{"ignore_above":256,"index":true,"type":"keyword"}}},"match_mapping_type":"string","match":"*"}}],"properties":{"exceptions":{"type":"nested","properties":{"Depth":{"type":"integer"},"ExceptionMessage":{"type":"object","properties":{"MemberType":{"type":"integer"}}},"HResult":{"type":"integer"},"RemoteStackIndex":{"type":"integer"},"RemoteStackTraceString":{"index":true,"type":"text"},"StackTraceString":{"index":true,"type":"text"}}},"message":{"index":true,"type":"text"}}}}}

步骤三：索引名称以 'serilog-jackprojecttemplate' 开头，剩下的部分系统会自动配置
*/
