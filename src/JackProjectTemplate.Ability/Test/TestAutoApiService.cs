﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackProjectTemplate.Ability.Docking.Dtos.TestDtos;
using JackProjectTemplate.Ability.Docking.Test;
using JackProjectTemplate.Domain.Data.Test;
using Youshow.Ace;
using Youshow.Ace.Ability;
using Youshow.Ace.Ability.Dtos;

namespace JackProjectTemplate.Ability.Test
{
    /// <summary>
    /// 测试自动api
    /// </summary>
    [NonRemoteVisible]
    public class TestAutoApiService : AutoAbilityServicer<TestEntity, TestDto, long, PageSortRequestDto, TestCreateDto, TestUpdateDto>, ITestAutoApiService
    {
    }
}
