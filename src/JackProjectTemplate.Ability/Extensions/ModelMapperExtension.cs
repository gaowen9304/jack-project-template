﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Youshow.Ace.ModelMapping
{
    public static class ModelMapperExtension
    {
        public static TDestination MapEx<TDestination>(this IModelMapper modelMapper, object source)
        {
            ArgumentNullException.ThrowIfNull(modelMapper);
            return modelMapper.Map<object, TDestination>(source);
        }
    }
}
