﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using JackProjectTemplate.Ability.Docking.Dtos.TestDtos;
using JackProjectTemplate.Domain.Data.Test;

namespace JackProjectTemplate.Ability
{
    public partial class JackProjectTemplateAbilityProfile
    {
        private void ConfigTestAutoApiService()
        {
            CreateMap<TestEntity, TestDto>();
            CreateMap<TestCreateDto, TestEntity>();
            CreateMap<TestUpdateDto, TestEntity>();
        }
    }
}
