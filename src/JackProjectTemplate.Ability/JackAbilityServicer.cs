﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using Youshow.Ace.Ability;

namespace JackProjectTemplate.Ability
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public abstract class JackAbilityServicer : AbilityServicer
    {
    }
}
